//3 & 4
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json())
.then( data => {
    console.log(data.map( element => element.title))
})

//5 & 6
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then( data => data.json())
.then( data => {
    console.log(data)
    console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
})

//7
fetch(`https://jsonplaceholder.typicode.com/todos`,{
    method: "POST",
    headers:{
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: false,
        title: "Create To Do List Item",
        userId: 1
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//8 & 9
fetch(`https://jsonplaceholder.typicode.com/todos/1`,{
    method: "PUT",
    headers:{
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update my to do list with a different data structure",
        status: "Pending",
        title: "Update To Do List Item",
        userId: 1
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//10 & 11
fetch(`https://jsonplaceholder.typicode.com/todos/1`,{
    method: "PATCH",
    headers:{
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        status: "Complete"
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})


//12
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "DELETE",
})